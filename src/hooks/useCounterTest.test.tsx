import { renderHook, act } from "@testing-library/react-hooks";
import { useCounter } from "./useCounter";

test("counter with initial", () => {
  const { result } = renderHook(() => useCounter(0));
  expect(result.current.count).toBe(0);
});

test("should increment counter", () => {
  const { result } = renderHook(() => useCounter(0));

  act(() => {
    result.current.increment();
  });
  expect(result.current.count).toBe(1);
});

test("should decrement counter", () => {
  const { result } = renderHook(() => useCounter(1));

  act(() => {
    result.current.decrement();
  });
  expect(result.current.count).toBe(0);
});

test("should be zero after increment and decrement", () => {
  const { result } = renderHook(() => useCounter(0));

  act(() => {
    result.current.increment();
    result.current.increment();
    result.current.decrement();
    result.current.decrement();
  });
  expect(result.current.count).toBe(0);
});
