import "./App.css";
import Counter from "./Counter/Counter";
import FetchComponent from "./FetchComponent/FetchComponent";
import LocalStorageComponent from "./LocalStorageComponent/LocalStorageBlock";

function App() {
  return (
    <div className="app-container">
      <Counter initialValue={0} />
      <FetchComponent />
      <LocalStorageComponent />
    </div>
  );
}

export default App;
