import { act, render, screen } from "@testing-library/react";
import LocalStorageComponent from "./LocalStorageBlock";

const setLocalStorage = (id, data) => {
  window.localStorage.setItem(id, JSON.stringify(data));
};

describe("App", () => {
  it("Test call with empty Local Storage", () => {
    act(() => {
      render(<LocalStorageComponent />);
    });

    expect(screen.queryByTestId("local-storage-view")).toEqual(null);
  });

  it("Test call with Local Storage data", () => {
    const mockId = "1";
    const mockJson = { data: "json data" };
    setLocalStorage(mockId, mockJson);
    act(() => {
      render(<LocalStorageComponent />);
    });

    expect(screen.queryByTestId("local-storage-view")).toHaveTextContent(
      JSON.stringify({ 1: '{"data":"json data"}' })
    );
  });
});
