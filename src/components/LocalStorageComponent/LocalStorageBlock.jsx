import React, { useCallback, useEffect, useState } from "react";

function LocalStorageComponent() {
  const [formData, setFormData] = useState({
    key: "",
    value: "",
  });

  const [localStorageData, setLocalStorageData] = useState(null);

  const formDataOnChange = useCallback((key, value) => {
    setFormData((prev) => ({ ...prev, [key]: value }));
  }, []);

  const getLocalStorage = useCallback(() => {
    setLocalStorageData(JSON.stringify(localStorage));
  }, []);

  const resetLocalStorage = useCallback(() => {
    localStorage.clear();
    setLocalStorageData(null);
  }, []);

  const onSubmit = useCallback(
    (e) => {
      e.preventDefault();

      localStorage.setItem(formData.key, formData.value);
    },
    [formData.key, formData.value]
  );

  useEffect(() => {
    if (localStorage.length > 0) getLocalStorage();
  }, [getLocalStorage]);

  return (
    <div>
      <form id="localStorage" onSubmit={onSubmit}>
        <input
          type="text"
          value={formData.key}
          onChange={(e) => formDataOnChange("key", e.target.value)}
        />
        <input
          type="text"
          value={formData.value}
          onChange={(e) => formDataOnChange("value", e.target.value)}
        />
        <button form="localStorage">Set to localStorage </button>
      </form>
      <div>
        {localStorageData && (
          <div data-testid="local-storage-view">{localStorageData}</div>
        )}
        <button onClick={getLocalStorage}>Get from localStorage </button>
        <button onClick={resetLocalStorage}>Clear localStorage </button>
      </div>
    </div>
  );
}
export default LocalStorageComponent;
