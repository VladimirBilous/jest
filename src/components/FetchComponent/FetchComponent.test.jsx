import React from "react";
import { render, screen } from "@testing-library/react";
import { act } from "react-dom/test-utils";
import FetchComponent from "./FetchComponent";

it("renders fetch data", async () => {
  const fakeUsersArray = [
    {
      body: "quia et suscipit\nsuscipit recusandae consequuntur expedita et cum\nreprehenderit molestiae ut ut quas totam\nnostrum rerum est autem sunt rem eveniet architecto",
      id: 1,
      title:
        "sunt aut facere repellat provident occaecati excepturi optio reprehenderit",
      userId: 1,
    },
    {
      body: "est rerum tempore vitae\nsequi sint nihil reprehenderit dolor beatae ea dolores neque\nfugiat blanditiis voluptate porro vel nihil molestiae ut reiciendis\nqui aperiam non debitis possimus qui neque nisi nulla",
      id: 2,
      title: "qui est esse",
      userId: 1,
    },
  ];

  jest.spyOn(global, "fetch").mockImplementation(() =>
    Promise.resolve({
      json: () => Promise.resolve(fakeUsersArray),
    })
  );

  await act(async () => {
    render(<FetchComponent />);
  });

  const fetchUsers = await screen.findAllByTestId("fetch-item");
  const fetchUserFirst = fetchUsers[0];
  const fetchUserSecond = fetchUsers[1];

  expect(fetchUsers.length).toBe(2);
  expect(fetchUserFirst).toHaveTextContent(
    "sunt aut facere repellat provident occaecati excepturi optio reprehenderit"
  );
  expect(fetchUserSecond.textContent).toBe("qui est esse");

  global.fetch.mockRestore();
});
