import React, { useEffect, useState } from "react";
import "./FetchComponent.css";

export function FetchComponent() {
  const [posts, setPosts] = useState([]);

  // console.log(posts);

  useEffect(() => {
    fetch("https://jsonplaceholder.typicode.com/posts")
      .then((response) => response.json())
      .then((json) => setPosts(json.slice(0, 10)));
  }, []);
  return (
    <div className="fetch-container">
      {posts.map((item) => (
        <div key={item.id} className="fetch-item" data-testid="fetch-item">
          {item.title}
        </div>
      ))}
    </div>
  );
}

export default FetchComponent;
