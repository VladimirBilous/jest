import { useEffect } from "react";
import { useCounter } from "../../hooks/useCounter";
import "./Counter.css";

function Counter({ initialValue }) {
  const { count, increment, decrement, reset } = useCounter(initialValue);

  // useEffect(() => {
  //   increment();
  // }, []);

  return (
    <div className="counter-container">
      <p data-testid="conter-value" className="counter-value">
        {count}
      </p>
      <div className="counter-controlers">
        <button data-testid="conter-reset" onClick={reset}>
          Reset
        </button>
        <button data-testid="conter-decrement" onClick={decrement}>
          Decrement
        </button>
        <button data-testid="conter-increment" onClick={increment}>
          Increment
        </button>
      </div>
    </div>
  );
}

export default Counter;
