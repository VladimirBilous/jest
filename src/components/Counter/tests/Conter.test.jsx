import React from "react";
import { create } from "react-test-renderer";
import Counter from "../Counter";
import { render, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";

describe("Counter snapshots", () => {
  test("Matches the snapshot", () => {
    const counter = create(<Counter />);
    expect(counter.toJSON()).toMatchSnapshot();
  });

  test("Matches the snapshot with initial value", () => {
    const counter = create(<Counter initialValue={1} />);
    expect(counter.toJSON()).toMatchSnapshot();
  });
});

const setup = (initialValue) =>
  render(<Counter initialValue={initialValue || 1} />);

const getCounterValue = () => screen.getByTestId("conter-value");
const getCounterResetButton = () => screen.getByTestId("conter-reset");
const getCounterDecrementButton = () => screen.getByTestId("conter-decrement");
const getCounterIncrementButton = () => screen.getByTestId("conter-increment");

const loopExecute = (loopCount, functionForCall) => {
  for (let i = 0; i < loopCount; i++) {
    userEvent.click(functionForCall);
  }
};

describe("Counter functionality", () => {
  it("should render a counter with value of 10", () => {
    setup(10);

    const basicCounter = getCounterValue();
    expect(basicCounter).toHaveTextContent(10);
  });

  it("should increment counter", () => {
    setup(10);

    const basicCounter = getCounterValue();
    const counterIncrement = getCounterIncrementButton();

    expect(basicCounter).toHaveTextContent(10);
    userEvent.click(counterIncrement);
    expect(basicCounter).toHaveTextContent(11);
    loopExecute(5, counterIncrement);
    expect(basicCounter).toHaveTextContent(16);
  });

  it("should decrement counter", () => {
    setup(10);

    const basicCounter = getCounterValue();
    const counterDecrement = getCounterDecrementButton();

    expect(basicCounter).toHaveTextContent(10);
    userEvent.click(counterDecrement);
    expect(basicCounter).toHaveTextContent(9);
    loopExecute(5, counterDecrement);
    expect(basicCounter).toHaveTextContent(4);
  });

  it("should reset counter", () => {
    setup(0);

    const basicCounter = getCounterValue();
    const counterReset = getCounterResetButton();
    const counterIncrement = getCounterIncrementButton();
    const counterDecrement = getCounterDecrementButton();

    expect(basicCounter).toHaveTextContent(1);
    loopExecute(5, counterIncrement);
    expect(basicCounter).toHaveTextContent(6);
    userEvent.click(counterReset);
    expect(basicCounter).toHaveTextContent(0);
    loopExecute(5, counterDecrement);
    expect(basicCounter).toHaveTextContent(-5);
    userEvent.click(counterReset);
    expect(basicCounter).toHaveTextContent(0);
  });
});
